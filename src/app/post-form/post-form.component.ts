import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {NgForm} from '@angular/forms';
import {Post} from '../post/post';

@Component({
  selector: 'my-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css']
})
export class PostFormComponent implements OnInit {
 
  @Output() postAddEvent=new EventEmitter<Post>();
  post:Post={id:'',title:''};
  constructor() { }

   onSubmit(form:NgForm){
    this.postAddEvent.emit(this.post)
    this.post={
      id:'',
      title:''
    };  
  }
  ngOnInit() {
  }

}
