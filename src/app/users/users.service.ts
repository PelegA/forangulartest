import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import {AngularFire} from 'angularfire2';
@Injectable()
export class UsersService {

  //private _url="http://jsonplaceholder.typicode.com/users";
  usersObservable;
  
constructor(private af:AngularFire) { }
 
 getUsers(){//populates this.usersObservable in the list
    this.usersObservable= this.af.database.list('/users').map( //map on observable array
      users=>{//this is already an array
        users.map( //map on regular array
          user=>{
            user.postTitles=[];
            //כל יוזר מקבל תכונה חדשה פוסט-טייטל שהוא מערך של אובסרבלס
            for(var p in user.posts){//because each user has an attr called "posts" in FireBase
              user.postTitles.push( //populates the array with the right posts
                this.af.database.object('/posts/'+p)
                //כל אחד מהאובסרבבל פונה אל הפוסט המתאים מהם 
                //ניתן יהיה לחלץ את הטייטל
              )
            }
         }
        );
        return users;
      }
    )
    return this.usersObservable;
  }

  addUser(user){ //add user can come only(!) after get the users list
    this.usersObservable.push(user); 
  }

  updateUser(user){
    let userKey=user.$key;
    let userData={name:user.name,email:user.email};
    this.af.database.object('/users/'+userKey).update(userData);
  }

  deleteUser(user){
    let userKey=user.$key;
    this.af.database.object('/users/'+userKey).remove();
  }
}

